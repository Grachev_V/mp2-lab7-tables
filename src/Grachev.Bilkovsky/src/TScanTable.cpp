// TScanTable.cpp
// Просматриваемые таблицы

#include "include/TScanTable.h"

PTDatValue TScanTable::FindRecord(TKey k)
{
    SetRetCode(TabOK);

    for (int i = 0; i < TabSize; ++i) {
        if (pRecs[i] != nullptr)
        if (pRecs[i]->GetKey() == k) {
            return pRecs[i]->GetValuePtr();
        }

    }
    SetRetCode(TabNoRec);
    return nullptr;
}

        /*-------------------------------------------*/

void TScanTable::InsRecord(TKey k, PTDatValue pVal)
{
    SetRetCode(TabNoRec);
    for (int i = 0; i < TabSize; ++i) {
        if (pRecs[i] == nullptr)
        {
            pRecs[i] = new TTabRecord(k, pVal);
            DataCount++;
            SetRetCode(TabOK);
            break;
        }
    }
}

        /*-------------------------------------------*/

void TScanTable::DelRecord(TKey k)
{
    SetRetCode(TabNoRec);
    for (int i = 0; i < TabSize; i++) {
        if (pRecs[i] != nullptr)
        if (pRecs[i]->GetKey() == k) {
            delete pRecs[i];
            pRecs[i] = pRecs[DataCount - 1];
            pRecs[DataCount - 1] = nullptr;
            DataCount--;
            SetRetCode(TabOK);
        }
    }
}
