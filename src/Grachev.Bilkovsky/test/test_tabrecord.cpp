#include <gtest/gtest.h>
#include "include/TTabRecord.h"
#include "src/TTabRecord.cpp"

TEST(TabRecord, Can_Create_TabRecord) {
    ASSERT_NO_THROW(TTabRecord rec1("abc"));
}

TEST(TabRecord, Can_Set_Key) {
    TTabRecord rec1;
    ASSERT_NO_THROW(rec1.SetKey("abc"));
}

TEST(TabRecord, Can_Get_Key) {
    string s = "cba";
    TTabRecord rec1("abc");
    
    rec1.SetKey(s);

    bool keys_equality = (s == rec1.GetKey()) && (rec1.GetKey() != "abc");

    ASSERT_TRUE(keys_equality);
}

TEST(TabRecord, Can_Set_Value) {
    TTabRecord rec1("abc");
    int val = 123;

    ASSERT_NO_THROW(rec1.SetValuePtr((PTDatValue)&val));
}

TEST(TabRecord, Can_Get_Value) {
    TTabRecord rec1("abc");
    int val = 123, expected_val;

    rec1.SetValuePtr((PTDatValue)&val);

    expected_val = *(int*)(rec1.GetValuePtr());

    EXPECT_EQ(val, expected_val);
}

TEST(TabRecord, Records_With_Equal_Keys_Are_Equal) {
    TTabRecord rec1("abc");
    TTabRecord rec2("abc");

    ASSERT_TRUE(rec1 == rec2);
}

TEST(TabRecord, Records_With_Not_Equal_Keys_Are_Not_Equal) {
    TTabRecord rec1("abc");
    TTabRecord rec2("cba");

    ASSERT_FALSE(rec1 == rec2);
}

TEST(TabRecord, Record_With_Bigger_Key_Is_Less) {
    TTabRecord rec1("aaa"), rec2("aab");

    ASSERT_TRUE(rec2 > rec1);
}

TEST(TabRecord, Record_With_Less_Key_Is_Less) {
    TTabRecord rec1("aaa"), rec2("aab");

    ASSERT_TRUE(rec1 < rec2);
}

TEST(TabRecord, Can_Assign_Record) {
    TTabRecord rec1("abc"), rec2;

    rec2 = rec1;

    ASSERT_TRUE(rec1 == rec2);
}

TEST(TabRecord, Assigned_Record_Has_Its_own_Mem) {
    TTabRecord rec1("abc"), rec2;

    rec2 = rec1;
    rec2.SetKey("cba");

    ASSERT_FALSE(rec1 == rec2);
}

