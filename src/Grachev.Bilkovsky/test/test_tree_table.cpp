#include <gtest/gtest.h>
#include <include/TTreeNode.h>
#include <src/TTreeNode.cpp>
#include <include/TBalanceNode.h>
#include <src/TBalanceNode.cpp>
#include "include/TTreeTable.h"
#include "src/TTreeTable.cpp"
#include "include/TBalanceTree.h"
#include "src/TBalanceTree.cpp"

TEST(TreeNode, Can_Create_Tree_Node) {
    ASSERT_NO_THROW(TTreeNode node());
}

TEST(TreeNode, Can_Place_Nodes_In_Tree) {
    int* i = new int(1);
    i[0] = 5;
    TTreeNode left("aaaa", (PTDatValue)&i[0]);
    i = new int(1);
    i[0] = 4;
    TTreeNode right("aaac", (PTDatValue)&i[0]);
    i = new int(1);
    i[0] = 10;
    TTreeNode Center("aaab", (PTDatValue)&i[0], &left, &right);

    EXPECT_EQ(Center.GetLeft(), &left);
    EXPECT_EQ(Center.GetRight(), &right);
}

TEST(BalanceNode, Can_Create_Balanced_Tree_Node) {
    EXPECT_NO_FATAL_FAILURE(TBalanceNode TBN);
}

TEST(TreeTable, Can_Create_Tree_Table) {
    EXPECT_NO_FATAL_FAILURE(TTreeTable table);
}

TEST(TreeTable, Can_Insert_Record) {
    int* i = new int(1);
    i[0] = 3;
    TTreeTable* table = new TTreeTable();
    ASSERT_NO_THROW(table->InsRecord("aaaa", (PTDatValue)&i[0]));
}

TEST(TreeTable, Can_Get_Key) {
    int* i = new int(1);
    i[0] = 3;
    TTreeTable* table = new TTreeTable();
    table->InsRecord("aaaa", (PTDatValue)&i[0]);
    table->Reset();
    EXPECT_EQ("aaaa", table->GetKey());
}

TEST(TreeTable, Can_Get_Value) {
    int* i = new int(1);
    i[0] = 3;
    TTreeTable* table = new TTreeTable();
    table->InsRecord("aaaa", (PTDatValue)&i[0]);
    table->Reset();
    EXPECT_EQ(3, *((int*)table->GetValuePtr()));
}

TEST(TreeTable, Can_Find_Record) {
    int* i = new int(1);
    i[0] = 3;
    TTreeTable* table = new TTreeTable();
    table->InsRecord("aaaa", (PTDatValue)&i[0]);
    i = new int(1);
    i[0] = 5;
    table->InsRecord("aaae", (PTDatValue)&i[0]);
    i = new int(1);
    i[0] = 2;
    table->InsRecord("aaad", (PTDatValue)&i[0]);

    EXPECT_EQ(*(int*)table->FindRecord("aaae"), 5);
}

TEST(BalanceTree, Can_Create_Balanced_Tree) {
    ASSERT_NO_THROW(TBalanceTree tree);
}

TEST(BalanceTree, Can_Add_And_Find_Record) {
    int* i = new int(1);
    i[0] = 3;
    TBalanceTree* table = new TBalanceTree();
    table->InsRecord("aaai", (PTDatValue)&i[0]);
    i = new int(1);
    i[0] = 5;
    table->InsRecord("aaba", (PTDatValue)&i[0]);
    i = new int(1);
    i[0] = 1;
    table->InsRecord("acaa", (PTDatValue)&i[0]);
    i = new int(1);
    i[0] = 2;
    table->InsRecord("aaab", (PTDatValue)&i[0]);
    i = new int(1);
    i[0] = 4;
    table->InsRecord("aage", (PTDatValue)&i[0]);

    EXPECT_EQ(*(int*)table->FindRecord("aaab"), 2);
}