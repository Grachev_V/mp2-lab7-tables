#include <gtest/gtest.h>
#include "include/TArrayTable.h"
#include "src/TArrayTable.cpp"
#include "include/TScanTable.h"
#include "include/TSortTable.h"
#include "src/TScanTable.cpp"
#include "src/TSortTable.cpp"

TEST(ScanTable, Can_Create_ScanTable) {
    ASSERT_NO_THROW(TScanTable table(5));
}

TEST(ScanTable, Can_Insert_Record) {
    TScanTable* table = new TScanTable(5);
    int i = 123;

    ASSERT_NO_THROW((table->InsRecord("abc", (TDatValue*)&i)));
}

TEST(ScanTable, Can_Get_Current_Record) {
    TScanTable* table = new TScanTable(5);
    int i = 123;
    TTabRecord rec, expected_rec("abc");

    (table->InsRecord("abc", (TDatValue*)&i));
    rec = *(table->GetCurrRecord());

    ASSERT_TRUE(rec == expected_rec);
}

TEST(ScanTable, Can_Get_Table_Size) {
    TScanTable* table = new TScanTable(5);

    EXPECT_EQ(5, table->GetTabSize());
}

TEST(ScanTable, Full_Table_Is_Full) {
    TScanTable* table = new TScanTable(5);
    int numbers[5];
    string s = "aaa";

    for (int i = 0; i < 5; i++) {
        char smb = 'a' + i;
        numbers[i] = i;
        table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
    }

    ASSERT_TRUE(table->IsFull());
}

TEST(ScanTable, Can_Set_Current_Position) {
    TScanTable* table = new TScanTable(5);
    int numbers[5];
    string s = "aaa";

    for (int i = 0; i < 5; i++) {
        char smb = 'a' + i;
        numbers[i] = i;
        table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
    }
    table->SetCurrentPos(2);

    ASSERT_TRUE("aaac" == table->GetKey());
}

TEST(ScanTable, Can_Get_Current_Position) {
    TScanTable* table = new TScanTable(5);
    int numbers[5];
    string s = "aaa";

    for (int i = 0; i < 5; i++) {
        char smb = 'a' + i;
        numbers[i] = i;
        table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
    }
    table->SetCurrentPos(3);

    EXPECT_EQ(3, table->GetCurrentPos());
}

TEST(ScanTable, Can_Reset_Position) {
    TScanTable* table = new TScanTable(5);
    int numbers[5];
    string s = "aaa";

    for (int i = 0; i < 5; i++) {
        char smb = 'a' + i;
        numbers[i] = i;
        table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
    }
    table->SetCurrentPos(3);
    table->Reset();

    EXPECT_EQ(0, table->GetCurrentPos());
}

TEST(ScanTable, Can_Go_Next_Record) {
    TScanTable* table = new TScanTable(5);
    int numbers[5];
    string s = "aaa";

    for (int i = 0; i < 5; i++) {
        char smb = 'a' + i;
        numbers[i] = i;
        table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
    }
    table->GoNext();
    table->GoNext();

    bool correctness = (2 == table->GetCurrentPos()) && 
                       ("aaac" == table -> GetKey());
    ASSERT_TRUE(correctness);
}

TEST(ScanTable, Can_Find_Record) {
    TScanTable* table = new TScanTable(5);
    int* i = new int(1);
    i[0] = 5;
    table->InsRecord("aaaa", (TDatValue*)&i[0]);
    i = new int(1);
    i[0] = 2;
    table->InsRecord("aaab", (TDatValue*)&i[0]);
    i = new int(1);
    i[0] = 3;
    table->InsRecord("aaba", (TDatValue*)i);
    i = new int(1);
    i[0] = 4;
    table->InsRecord("aabc", (TDatValue*)i);

    EXPECT_EQ(2, *((int*)(table->FindRecord("aaab"))));
}

TEST(ScanTable, Can_Delete_Record) {
    TScanTable* table = new TScanTable(5);
    int numbers[5];
    string s = "aaa";

    for (int i = 0; i < 5; i++) {
        char smb = 'a' + i;
        numbers[i] = i;
        table->InsRecord(s + smb, (TDatValue*)&(numbers[i]));
    }

    ASSERT_NO_THROW(table->DelRecord("aaac"));
    EXPECT_TRUE(nullptr == table->FindRecord("aaac"));
}

TEST(SortTable, Can_Assign_Table) {
        TSortTable table(5), atable(1);
        int numbers[5], n = 5;
        string s = "aaa";

        for (int i = 0; i < 5; i++) {
            char smb = 'a' + i;
            numbers[i] = i;
            table.InsRecord(s + smb, (TDatValue*)&(numbers[i]));
        }
        atable.InsRecord("aaa", (PTDatValue)&n);

    ASSERT_NO_THROW(atable = table);
}

TEST(SortTable, Assigned_Table_Has_Its_Own_Mem) {
    TSortTable table(5), atable(1);
    int numbers[5], n = 5;
    string s = "aaa";

    for (int i = 0; i < 5; i++) {
        char smb = 'a' + i;
        numbers[i] = i;
        table.InsRecord(s + smb, (TDatValue*)&(numbers[i]));
    }
    atable.InsRecord("aaa", (PTDatValue)&n);
    atable = table;
    table.DelRecord("aaac");

    bool correctness = (table.FindRecord("aaac") == nullptr) &&
                       (3, *((int*)(atable.FindRecord("aaac"))));
    ASSERT_TRUE(correctness);
}

TEST(SortTable, Can_Set_And_Get_Sort_Method) {
    TSortTable* table = new TSortTable(5);
    int i = 5;
    table->SetSortMethod(INSERT_SORT);

    EXPECT_TRUE(table->GetSortMethod() == INSERT_SORT);
}

TEST(SortTable, Can_Use_Different_Sort_Methods) {
    TSortTable* table = new TSortTable(5);
    int* i = new int(1);
    i[0] = 5;
    table->SetSortMethod(INSERT_SORT);
    ASSERT_NO_THROW(table->InsRecord("aaaa", (TDatValue*)&i[0]));
    i = new int(1);
    i[0] = 4;
    table->SetSortMethod(MERGE_SORT);
    ASSERT_NO_THROW(table->InsRecord("aaab", (TDatValue*)&i[0]));
    i = new int(1);
    i[0] = 3;
    table->SetSortMethod(QUICK_SORT);
    ASSERT_NO_THROW(table->InsRecord("aaac", (TDatValue*)&i[0]));
    i = new int(1);
    i[0] = 4;
    table->SetSortMethod(INSERT_SORT);
    ASSERT_NO_THROW(table->InsRecord("aaad", (TDatValue*)&i[0]));

    EXPECT_EQ(3, *((int*)(table->FindRecord("aaac"))));
}